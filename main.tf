provider "aws" {
  region = "us-east-1"
  profile = "book"
}

resource "aws_instance" "example" {
  ami = "ami-09d3b3274b6c5d4aa"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ aws_security_group.instance.id ]
  user_data = <<-EOF
                #!/bin/bash
                echo "Hello World" > index.html
                nohup busybox httpd -f -p 8080 &
              EOF
  #user_data_replace_on_change = true
  tags = {
    Name = "terraform-example"
  }
}

resource "aws_security_group" "instance" {
  name = "terraform-example-instance"
  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
  }
}